# Plotly for Julia

This repository contains a template signin file to use [Plotly](https://plot.ly/) from [the Julia Language](http://julialang.org/).

Copy the [plotly_signin.jl](https://bitbucket.org/npalmius/plotly-julia/src/master/plotly_signin.jl) to the project folder and configure the username and password for the Plotly account to use.

If the template is added to git, ignore the changes by running

    git update-index --assume-unchanged plotly_signin.jl

To track changes again run

    git update-index --no-assume-unchanged plotly_signin.jl

---

[![Creative Commons Licence](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

Source files are licensed under the [BSD 3-Clause License](http://opensource.org/licenses/BSD-3-Clause).