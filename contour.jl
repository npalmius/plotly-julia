# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 1-Oct-2014

function contour(data)
	contour([0:(size(data, 1) - 1)], [0:(size(data, 2) - 1)], data);
end

function contour(x, y, z)
	contourf(x, y, z);
end

function contourf(data)
	contourf([0:(size(data, 1) - 1)], [0:(size(data, 2) - 1)], data);
end

function contourf(x, y, z)
	plot_data = [
	    "x" => x,
	    "y" => y,
	    "z" => z,
	    "type" => "contour"
	];

	response = try
	    Plotly.plot([plot_data], ["filename" => "temp-contour", "fileopt" => "overwrite"]);
	catch
	    {"url" => "Error sending plot."};
	end
	print("Plotting temporary contour : ");
	plot_url = response["url"];
	println(plot_url);
end
