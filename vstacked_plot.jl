# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 5-Oct-2014

function vstacked_plot(data)
	if size(data[:,1]) == (1,)
		vstacked_plot([0:(size(data[:,1][1], 1) - 1)], data)
	else
		vstacked_plot([0:(size(data, 1) - 1)], data)
	end
end

function vstacked_plot(x, y)
	data = [];
	layout = Dict();

	plots = size(y, 2);

	for i = 1:plots
		if size(y[:,i]) == (1,)
			a = y[:,i][1];
			lines = 1:size(a, 2);
		else
			a = y;
			lines = i;
		end

		for j = lines
			plot_data = [
			    "x" => x,
			    "y" => a[:,j],
			    "xaxis" => string("x", i),
			    "yaxis" => string("y", i),
			    "mode" => "lines",
			    "line" => ["shape" => "linear"],
			    "type" => "scatter"
			];
			if length(data) == 0
				data = [plot_data];
			else
				data = [data, plot_data];
			end
		end

		axis = "axis";

		if i > 1
			axis = string(axis, i);
			layout[string("x", axis)] = ["anchor" => string("y", i)];
		end

		v_space = 0.1;

		sp = v_space / (1 + v_space);

		domain = min([((plots - i) / plots)  (((plots - i + 1) / plots) - sp)] / (1 - sp), 1);

		layout[string("y", axis)] = ["domain" => domain];
	end

	response = try
	    Plotly.plot([data], ["layout" => layout, "filename" => "temp-plot", "fileopt" => "overwrite"]);
	catch
	    {"url" => "Error sending plot."};
	end
	print("Plotting temporary plot : ");
	plot_url = response["url"];
	println(plot_url);
end
